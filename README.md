<div align="center">

# Chumsky-Vola

**VO**lume **LA**nguage: A research [DSL](https://en.wikipedia.org/wiki/Domain-specific_language) that evaluates volume functions.

Examples of such functions are [Signed Distance Functions](https://en.wikipedia.org/wiki/Signed_distance_function) or [Unit Gradient Fields](https://www.blakecourter.com/2023/05/18/field-notation.html).
The implementation will focus on SDFs at first.

[![dependency status](https://deps.rs/repo/gitlab/tendsinmende/chumsky-vola/status.svg)](https://deps.rs/repo/gitlab/tendsinmende/chumsky-vola)
[![MPL 2.0](https://img.shields.io/badge/License-MPL_2.0-blue)](LICENSE)

</div>

The [Chumsky](https://github.com/zesterer/chumsky) implementation of [Vola's]() syntax.


## Building


``` shell
cargo build

```

## Testing
``` shell
cargo test
```


## License

Licensed under

Mozilla Public License Version 2.0 ([LICENSE](LICENSE) or <https://www.mozilla.org/en-US/MPL/2.0/>)
